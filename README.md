# Droppy Czech

Český překlad webové aplikace pro sdílení souborů **Droppy**

https://droppy.proxibolt.com/

## Použití

1. V administraci Droppy na záložce _Settings_ a podzáložce _Language_ přidejte nový překlad.
_Language name_  = Čeština, _Directory name_ = czech, _Locale_ = cs_CZ
2. Z repozitáře si stáhněte složku _czech_.
3. Obsah složky _czech_ (soubory _index.html_ a _main_lang.php_) umístěte do složky \application\language\czech\ na vašem serveru.
4. Zkontrolujte, zda má soubor _main_lang.php_ nastaven kódovou stránku _Unicode UTF-8 no BOM (65001)_
5. Nyní je možné překlad přepnout buď v uživatelské aplikaci nebo nastavit jako výchozí (Set default) v administračním panelu.

## Informace o verzi a změnách

Překlad a návod je aktuální pro Droppy verze 2.6.2. Použití s vyššími verzemi je možné, případné novinky budou ale pochopitelně v angličtině.
